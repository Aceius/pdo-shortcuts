<?php

/**
 * Simple class to adhere to DRY while writing PDO statements.
 * @author AceiusIO
 */
class PDOShortcuts
{
    public $dburl;

    function __construct(string $dburl)
    {
        $this->dburl = $dburl;
    }

    /**
     * Executes the given string as SQL. THIS FUNCTION IS VULNRABLE TO SQL INJECTION!
     *
     * Only use this when you are sure you trust input, or executing a fixed command.
     * Returns an array with the results, or a string containing an error message on failure.
     *
     * @param string $code SQL to execute.
     *
     * @return mixed
     */

    function execute(string $code)
    {
        $conn = new PDO($this->dburl);
        try {
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare($code);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            return "Database Error: " . $e->getMessage();
        }
    }

    /**
     * Executes the given string as SQL, using the array to prepare the statement.
     *
     * Returns an array with the results, or a string containing an error message on failure.
     *
     * @param string $code SQL to execute.
     * @param array $values Values to subsitute in. Entry with key :val will replace :val in $code.
     *
     * @return mixed
     */
    function prepared(string $code, array $values)
    {
        try {
            $conn = new PDO($this->dburl);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare($code);
            $stmt->execute($values);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }
}
